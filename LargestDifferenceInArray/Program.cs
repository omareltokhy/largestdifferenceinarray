﻿using System;

namespace LargestDifferenceInArray
{
    class Program
    {
        static void Main(string[] args)
        {
            //Asking array numbers from user
            Console.WriteLine("Enter 7 numbers");
            int num1 = Convert.ToInt32(Console.ReadLine());
            int num2 = Convert.ToInt32(Console.ReadLine());
            int num3 = Convert.ToInt32(Console.ReadLine());
            int num4 = Convert.ToInt32(Console.ReadLine());
            int num5 = Convert.ToInt32(Console.ReadLine());
            int num6 = Convert.ToInt32(Console.ReadLine());
            int num7 = Convert.ToInt32(Console.ReadLine());

            //Setting up variables from user data
            int[] maxMin = {num1, num2, num3, num4, num5, num6, num7 };
            int biggestElement = 0;
            int smallestElement = maxMin[0];
            int smallest = 0;
            int biggest = 0;

            //Finding lowest array number
            for (int i = 0; i < maxMin.Length; i++)
            {
                if (maxMin[i] < smallestElement)
                {
                    smallest = i;
                    smallestElement = maxMin[i];
                }
            }
            //Finding largest array number 
            for(int i = smallest; i < maxMin.Length; i++)
            {
                if (maxMin[i] > biggestElement)
                {
                    biggest = i;
                    biggestElement = maxMin[i];
                }
            }
            //Print result
            Console.WriteLine("The largest difference is:\n"+(biggestElement-smallestElement));
            Console.WriteLine("Explanation:\n"+(biggestElement-smallestElement)+" = "+biggestElement+"("+biggest+")"+"-"+smallestElement+"("+smallest+")");
        }
    }
}
